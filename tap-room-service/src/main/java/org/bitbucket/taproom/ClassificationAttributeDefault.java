package org.bitbucket.taproom;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Allows a set of queries to be executed, resulting in a set of defaults being applied to one of more fields.
 */
@JsonInclude
public class ClassificationAttributeDefault {

    /**
     * Defaults that should be applied when one of the query terms is matched.
     */
    private List<DefaultKeyValuePair> defaults;

    /**
     * Query teerms that will be OR-ed together to find appropriate records on which to apply defaults.
     */
    private List<String> queries;

    public List<DefaultKeyValuePair> getDefaults() {
        return defaults;
    }

    public void setDefaults(List<DefaultKeyValuePair> defaults) {
        this.defaults = defaults;
    }

    public List<String> getQueries() {
        return queries;
    }

    public void setQueries(List<String> queries) {
        this.queries = queries;
    }

}
