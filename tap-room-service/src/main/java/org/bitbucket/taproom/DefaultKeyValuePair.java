package org.bitbucket.taproom;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Specifies the default values to set for a given attribute.
 */
@JsonInclude
public class DefaultKeyValuePair {

    /**
     * Name of the field to default.
     */
    private String fieldName;

    /**
     * Default value to set on the field name.
     */
    private String defaultValue;

    /**
     * Whether or not to override a value if one is already set.
     */
    private boolean overwrite = false;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

}
