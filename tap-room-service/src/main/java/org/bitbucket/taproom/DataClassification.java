package org.bitbucket.taproom;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.aeonbits.owner.KrauseningConfigFactory;
import org.bitbucket.taproom.config.TapRoomConfig;
import org.bitbucket.taproom.lucene.client.IndexItem;
import org.bitbucket.taproom.lucene.client.TapRoomClientException;
import org.bitbucket.taproom.lucene.client.UnitOfWork;
import org.bitbucket.taproom.lucene.client.UnitOfWorkManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Provides abstract data classification functionality. The intent is that this class will be subclassed for each
 * classification routine you intent to run.
 * 
 * It will automatically call any public method that follows (first) an add*Attributes() and (second)
 * default*Attributes() naming convention.
 * 
 * You can also configure defaulting by adding one or more json files conforming to the ClassificationAttributeDefault
 * json contract.
 * 
 * @param <T>
 *            a subclass of {@link IndexItem} with the specific getters and logic needed for your classification routine
 */
public abstract class DataClassification<T extends IndexItem> {

    private static final Logger logger = LoggerFactory.getLogger(DataClassification.class);

    private static UnitOfWork unitOfWork = UnitOfWorkManager.getUnitOfWork();

    private static TapRoomConfig config = KrauseningConfigFactory.create(TapRoomConfig.class);
    private int attributesDefaultsQuerySize;   

    /**
     * Calls and add*Attributes() method to perform classification.
     */
    public void classifyData() {
        attributesDefaultsQuerySize = config.getDefaultsQueryPageSize();
        
        invokeAttributeClassificationByNamingConvention("add");

        // flush to make sure that transitive values (e.g., numeric settings) are available for defaulting:
        unitOfWork.commit();

        invokeAttributeClassificationByNamingConvention("default");

        // TODO: tie this into a transaction so we don't have to manually do this and make all classification methods
        unitOfWork.commit();
    }

    /**
     * Reads external configuration files and processes defaulting for a specific field, setting the values of that
     * field based on matches to the provided queries.
     */
    public void defaultExternallyConfiguredAttributes() {
        String attributeLocation = config.getAttributeDefaultLocation();
        File attributeDirectory = new File(attributeLocation);
        if (!attributeDirectory.exists()) {
            logger.warn("The specified attribute default configurations location does not exist: {}",
                    getCanonicalPath(attributeDirectory));

        } else {
            ObjectMapper mapper = new ObjectMapper();

            File[] attributeDefaultConfigurations = attributeDirectory.listFiles();
            if (attributeDefaultConfigurations != null) {
                for (File attributeDefaultConfiguration : attributeDefaultConfigurations) {
                    processAttributeDefault(mapper, attributeDefaultConfiguration);
                }
            }
        }
    }

    private void processAttributeDefault(ObjectMapper mapper, File attributeDefaultConfiguration) {
        ClassificationAttributeDefault[] attributeDefaults = null;
        try {
            attributeDefaults = mapper.readValue(attributeDefaultConfiguration, ClassificationAttributeDefault[].class);

        } catch (IOException e) {
            throw new TapRoomClientException("Could not read attribute defaults in file: "
                    + getCanonicalPath(attributeDefaultConfiguration) + "!", e);
        }

        if (attributeDefaults != null) {
            for (ClassificationAttributeDefault attributeDefault : attributeDefaults) {
                queryAndProcessMatches(attributeDefault);
            }
        }
    }

    private void queryAndProcessMatches(ClassificationAttributeDefault attributeDefault) {
        StringBuilder queryBuilder = new StringBuilder();
        List<String> queries = attributeDefault.getQueries();
        if (queries == null) {
            queryBuilder.append("*:*");
            logger.debug("By not specifying any query terms, all fields and all values will be queried!");

        } else {
            for (String queryTerm : queries) {
                if (queryBuilder.length() != 0) {
                    queryBuilder.append(" OR ");

                }

                queryBuilder.append(queryTerm);
            }

            String finalAttributeQuery = queryBuilder.toString();
            Collection<T> queryHits = executeQuery(finalAttributeQuery, 0, attributesDefaultsQuerySize);

            processQueryMatches(attributeDefault, queryHits);
        }
    }

    private void processQueryMatches(ClassificationAttributeDefault attributeDefault, Collection<T> queryHits) {
        for (T queryHit : queryHits) {
            Map<String, Object> values = queryHit.getValues();
            for (DefaultKeyValuePair defaulyKeyValue : attributeDefault.getDefaults()) {
                setValue(values, defaulyKeyValue);
            }

            queryHit.save();
        }
    }

    private void setValue(Map<String, Object> values, DefaultKeyValuePair defaulyKeyValue) {
        String fieldName = defaulyKeyValue.getFieldName();
        String defaultValue = defaulyKeyValue.getDefaultValue();
        if (values.containsKey(fieldName)) {
            if (defaulyKeyValue.isOverwrite() || values.get(fieldName) == null) {
                values.put(fieldName, defaultValue);
            }

        } else {
            values.put(fieldName, defaultValue);

        }
    }

    protected abstract Collection<T> executeQuery(String query, long startIndex, long pageSize);

    /**
     * Exception handling for getting canonical paths for easier debugging.
     * 
     * @param file
     *            the canonical path you are looking for
     * @return the value as a string, with any exception encountered thrown as a runtime exception
     */
    private String getCanonicalPath(File file) {
        try {
            return file.getCanonicalPath();

        } catch (IOException e) {
            throw new TapRoomClientException("Could not access file!", e);

        }
    }

    private void invokeAttributeClassificationByNamingConvention(String methodStartsWith) {
        Object[] params = null;
        Method[] methods = this.getClass().getMethods();
        for (Method method : methods) {
            String methodName = method.getName();
            if ((methodName.startsWith(methodStartsWith)) && (methodName.endsWith("Attributes"))
                    && (method.getParameterTypes().length == 0)) {

                try {
                    method.invoke(this, params);

                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                    logger.error("Could not invoke classification method: " + methodName, e);
                }
            }
        }
    }

}
