package org.bitbucket.taproom;

import java.io.File;
import java.util.List;

import org.aeonbits.owner.KrauseningConfigFactory;
import org.apache.commons.io.FilenameUtils;
import org.bitbucket.taproom.config.TapRoomConfig;
import org.bitbucket.taproom.lucene.client.IndexItemDao;
import org.bitbucket.taproom.lucene.client.TapRoomClientException;
import org.bitbucket.taproom.lucene.client.UnitOfWork;
import org.bitbucket.taproom.lucene.client.UnitOfWorkManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to assist in loading or "tapping" data to subsequent classification and transformation.
 */
public class DataLoad {

    private static final Logger logger = LoggerFactory.getLogger(DataLoad.class);

    private static UnitOfWork unitOfWork = UnitOfWorkManager.getUnitOfWork();
    private static TapRoomConfig config = KrauseningConfigFactory.create(TapRoomConfig.class);

    private IndexItemDao<?> dao = new IndexItemDao<>();

    /**
     * Ingests files contained within from the passed location.
     * 
     * @param fileLocation
     *            location containing files to be ingested
     */
    public void ingestFiles(String fileLocation) {
        int totalFilesIngested = 0;
        File directoryAsFile = new File(fileLocation);

        if (directoryAsFile.isDirectory()) {
            int filesIngested = streamFiles(directoryAsFile);

            totalFilesIngested = filesIngested;

        } else {
            logger.error("{} is not a directory!  Please pass a directory containing the files you want to load!",
                    fileLocation);

        }

        unitOfWork.commit();

        logger.debug("Ingested {} files", totalFilesIngested);
    }

    /**
     * Ingests all passed files with the provided extension from the passed location.
     * 
     * @param directory
     *            location containing files to be ingested
     * @return the number of ingested files
     */
    protected int streamFiles(File directory) {
        logger.debug("Starting ingestion of files from '{}'...", directory);

        int filesEncountered = 0;
        int filesIngested = 0;

        File[] labelsInDirectory = directory.listFiles();

        boolean success;
        for (File label : labelsInDirectory) {
            filesEncountered++;
            if (!".DS_Store".equalsIgnoreCase(label.getName())) {
                success = streamFile(label);
                if (success) {
                    filesIngested++;
                }

                outputProgressAndPossiblyCommit(filesEncountered);

            }
        }

        logger.debug("Ingested {}/{} files from '{}'", filesIngested, filesEncountered, directory);

        return filesIngested;
    }

    protected void outputProgressAndPossiblyCommit(int filesEncountered) {
        if (filesEncountered % 50 == 0) {
            logger.debug("Processed {} files", filesEncountered);
            if (filesEncountered % 1000 == 0) {
                unitOfWork.commit();
            }
        }
    }

    protected boolean streamFile(File label) {
        boolean success;
        String labelName;
        String fileExtension;
        labelName = label.getName();
        fileExtension = FilenameUtils.getExtension(labelName);
        String contentType;
        List<String> supportedExtensionTypes = config.getIngestFileExtensionTypes();

        contentType = getContentType(supportedExtensionTypes, fileExtension);
        if (contentType == null) {
            throw new TapRoomClientException("Unsupported file type. Expecting file type " + supportedExtensionTypes
                    + ", but found " + fileExtension);
        }
        success = dao.addToIndex(label, contentType);
        return success;
    }

    private String getContentType(List<String> supportedExtensionTypes, String fileExtension) {
        String contentType = null;

        for (String supportedExtensionType : supportedExtensionTypes) {
            if (supportedExtensionType.contains("/" + fileExtension)) {
                contentType = supportedExtensionType;
            }
        }
        return contentType;

    }

}
