package org.bitbucket.taproom.config;

import java.util.List;

import org.aeonbits.owner.KrauseningConfig;
import org.aeonbits.owner.KrauseningConfig.KrauseningSources;

/**
 * External configuration options for tap room.
 */
@KrauseningSources("tap-room.properties")
public interface TapRoomConfig extends KrauseningConfig {

    /**
     * The location from which to load attribute default configurations.
     * 
     * @return path
     */
    @Key("attribute.default.location")
    @DefaultValue("attribute-defaults")
    String getAttributeDefaultLocation();

    /**
     * Page size for the attribute defaults query
     * 
     * @return the page size for the attribute defaults queries
     */
    @Key("attribute.defaults.query.pagesize")
    @DefaultValue("2000")
    Integer getDefaultsQueryPageSize();
    
    /**
     * File extension type for data load
     * @return file extension type
     */
    @Key("ingest.file.extension.types")
    @DefaultValue("application/csv, application/json")
    List<String> getIngestFileExtensionTypes();

}
