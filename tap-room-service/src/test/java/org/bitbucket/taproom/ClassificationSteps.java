package org.bitbucket.taproom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomUtils;
import org.bitbucket.taproom.lucene.client.IndexItemDao;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ClassificationSteps {

    private IndexItemDao<BeerRating> dao = new IndexItemDao<>();

    private File csvDirectory = new File("target/csv");
    private Collection<BeerRating> ratings = new ArrayList<>();

    @After("@classifyData")
    public void cleanUp() throws Exception {
        dao.delete("*:*");

        if (csvDirectory.exists()) {
            FileUtils.forceDelete(csvDirectory);
        }
    }

    @Given("^an index classified to process full state names as synonyms with state abbreviations$")
    public void an_index_classified_to_process_full_state_names_as_synonyms_with_state_abbreviations()
            throws Throwable {
        // this is done in the index configuration (e.g, docker) via synonyms.txt
    }

    @Given("^an index classified to process brewery names so that matches to \"([^\"]*)\" have the following values defaulted:$")
    public void an_index_classified_to_process_brewery_names_so_that_matches_to_have_the_following_values_defaulted(
            String breweryName, List<String> classifiedValuesToBeDefaulted) throws Throwable {
        // see the src/test/resources/attribute-defaults/brewery-names.json for how this mapping is configured
    }

    @When("^a csv file is loaded for classification$")
    public void a_csv_file_is_loaded_for_classification() throws Throwable {
        DataLoad dataLoad = new DataLoad();
        dataLoad.ingestFiles("src/test/resources/test-data");
    }

    @When("^a csv file is loaded and classified that contains the state name \"([^\"]*)\"$")
    public void a_csv_file_is_loaded_and_classified_that_contains_the_state_name(String stateName) throws Throwable {
        BeerRating beerRating = createRandomBeerRating();
        beerRating.setState(stateName);
        ratings.add(beerRating);

        createCsvLoadAndClassifyData();

    }

    @When("^a csv file is loaded and classified that contains the brewery name \"([^\"]*)\"$")
    public void a_csv_file_is_loaded_and_classified_that_contains_the_brewery_name(String breweryName)
            throws Throwable {
        BeerRating beerRating = createRandomBeerRating();
        beerRating.setBrewery(breweryName);
        ratings.add(beerRating);

        createCsvLoadAndClassifyData();
    }

    @Then("^it is available for classification$")
    public void it_is_available_for_classification() throws Throwable {
        ratings = dao.query("sourceFile:beer-ratings.csv", 0, 100, BeerRating.class);
        assertTrue("Ratings should have been returned!", ratings.size() > 0);
    }

    @Then("^each row in the csv file is available as a unique document$")
    public void each_row_in_the_csv_file_is_available_as_a_unique_document() throws Throwable {
        assertEquals("Unexpected number of results encountered!", 5, ratings.size());
    }

    @Then("^the document is classified with the state abbreviation \"([^\"]*)\"$")
    public void the_document_is_classified_with_the_state_abbreviation(String stateAbbreviation) throws Throwable {
        ratings = dao.query("classifiedState:" + stateAbbreviation, 0, 100, BeerRating.class);
        assertEquals("One rating should have been returned!", 1, ratings.size());
        assertEquals("Rating did not have expected classified state!", stateAbbreviation,
                ratings.iterator().next().getClassifiedState());
    }

    @Then("^the document contains the following brewery information values defaulted:$")
    public void the_document_contains_the_following_brewery_information_values_defaulted(List<BeerRatingInput> expectedValuesList) throws Throwable {
        assertEquals("More expected results than expected returned!", 1, expectedValuesList.size());
        BeerRatingInput expectedValues = expectedValuesList.iterator().next();
        
        ratings = dao.query("classifiedBrewery:" + expectedValues.classifiedBrewery, 0, 100, BeerRating.class);        
        assertEquals("More results than expected returned!", 1, ratings.size());
        BeerRating result = ratings.iterator().next();        
        
        assertEquals("Unexpected brewery name!", expectedValues.classifiedBrewery, result.getClassifiedBrewery());
        assertEquals("Unexpected city name!", expectedValues.classifiedCity, result.getClassifiedCity());
        assertEquals("Unexpected state name!", expectedValues.classifiedState, result.getClassifiedState());
        assertEquals("Unexpected country name!", expectedValues.classifiedCountry, result.getClassifiedCountry());
        
    }

    private void createCsvLoadAndClassifyData() throws Throwable, IOException {
        createTestCsvFile(ratings);

        DataLoad dataLoad = new DataLoad();
        dataLoad.ingestFiles(csvDirectory.getCanonicalPath());

        BeerRatingClassification beerRatingClassification = new BeerRatingClassification();
        beerRatingClassification.classifyData();
    }

    private void createTestCsvFile(Collection<BeerRating> ratings) throws Throwable {
        BeerRatingCsvFile csvFileContent = new BeerRatingCsvFile();
        csvFileContent.records = ratings;

        csvDirectory.mkdirs();

        File csvFile = File.createTempFile("test-rating-", ".csv", csvDirectory);
        Writer writer = new FileWriter(csvFile);
        writer.write(csvFileContent.toString());
        writer.close();
    }

    private BeerRating createRandomBeerRating() {
        BeerRating beerRating = new BeerRating();

        beerRating.setId(UUID.randomUUID().toString());
        beerRating.setBrewery("Ballast Point");
        beerRating.setName("Sculpin");
        beerRating.setAbv("7%");
        beerRating.setIbu("70");
        beerRating.setCity("San Diego");
        beerRating.setState("California");
        beerRating.setCountry("USA");
        beerRating.setRating("5");
        beerRating.setUser("RandomUser-" + RandomUtils.nextInt());

        return beerRating;
    }

}