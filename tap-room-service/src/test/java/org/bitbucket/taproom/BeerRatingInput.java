package org.bitbucket.taproom;

public class BeerRatingInput {

    public String rawBrewery;
    public String classifiedBrewery;
    public String classifiedCity;
    public String classifiedState;
    public String classifiedCountry;
    
}
