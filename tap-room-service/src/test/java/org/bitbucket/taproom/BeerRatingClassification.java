package org.bitbucket.taproom;

import java.util.Collection;

import org.bitbucket.taproom.lucene.client.IndexItemDao;

/**
 * Example of how to implement a {@link DataClassification}.
 */
public class BeerRatingClassification extends DataClassification<BeerRating> {

    private IndexItemDao<BeerRating> dao = new IndexItemDao<>();

    public void addStateAttributes() {
        // Notional list of states - this would likely be read from some database or similar in real life:
        String[] stateAbbreviations = { "CA", "DE", "MN", "TN", "VA" };

        for (String stateAbbreviation : stateAbbreviations) {
            String query = "state:" + stateAbbreviation;
            Collection<BeerRating> matches = dao.query(query, 0, 1000, BeerRating.class);
            for (BeerRating beerRating : matches) {
                beerRating.setClassifiedState(stateAbbreviation);
                beerRating.save();

            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Collection<BeerRating> executeQuery(String query, long startIndex, long pageSize) {
        return dao.query(query, 0, 1000, BeerRating.class);
    }

}
