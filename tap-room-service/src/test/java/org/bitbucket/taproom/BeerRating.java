package org.bitbucket.taproom;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bitbucket.taproom.lucene.client.IndexItem;
import org.bitbucket.taproom.lucene.client.IndexItemDao;

/**
 * Example implementation for an {@link IndexItem} around rating a beer.
 */
public class BeerRating extends IndexItem {

    private static IndexItemDao<BeerRating> dao = new IndexItemDao<>();

    private static final String ID = "id";
    private static final String BREWERY = "brewery";
    private static final String NAME = "name";
    private static final String ABV = "abv";
    private static final String IBU = "ibu";
    private static final String CITY = "city";
    private static final String STATE = "state";
    private static final String COUNTRY = "country";
    private static final String RATING = "rating";
    private static final String USER = "user";

    private static final String CLASSIFIED_BREWERY = "classifiedBrewery";
    private static final String CLASSIFIED_NAME = "classifiedName";
    private static final String CLASSIFIED_ABV = "classifiedAbv";
    private static final String CLASSIFIED_IBU = "classifiedIbu";
    private static final String CLASSIFIED_CITY = "classifiedCity";
    private static final String CLASSIFIED_STATE = "classifiedState";
    private static final String CLASSIFIED_COUNTRY = "classifiedCountry";
    private static final String CLASSIFIED_RATING = "classifiedRating";
    private static final String CLASSIFIED_USER = "classifiedUser";

    private static Collection<String> indexedFields;

    public BeerRating() {
        super();
    }

    public BeerRating(Map<String, Object> rawIndexValues) {
        super(rawIndexValues);

    }

    @Override
    public Collection<String> getIndexedFields() {
        if (indexedFields == null) {
            indexedFields = new ArrayList<>();
            indexedFields.add(ID);
            indexedFields.add(BREWERY);
            indexedFields.add(NAME);
            indexedFields.add(ABV);
            indexedFields.add(IBU);
            indexedFields.add(CITY);
            indexedFields.add(STATE);
            indexedFields.add(COUNTRY);
            indexedFields.add(RATING);
            indexedFields.add(USER);

            indexedFields.add(CLASSIFIED_BREWERY);
            indexedFields.add(CLASSIFIED_NAME);
            indexedFields.add(CLASSIFIED_ABV);
            indexedFields.add(CLASSIFIED_IBU);
            indexedFields.add(CLASSIFIED_CITY);
            indexedFields.add(CLASSIFIED_STATE);
            indexedFields.add(CLASSIFIED_COUNTRY);
            indexedFields.add(CLASSIFIED_RATING);
            indexedFields.add(CLASSIFIED_USER);
        }

        return indexedFields;
    }

    public String getId() {
        return (String) values.get(ID);
    }

    public void setId(String id) {
        values.put(ID, id);
    }

    public String getBrewery() {
        return (String) values.get(BREWERY);
    }

    public void setBrewery(String brewery) {
        values.put(BREWERY, brewery);
    }

    public String getName() {
        return (String) values.get(NAME);
    }

    public void setName(String name) {
        values.put(NAME, name);
    }

    public String getAbv() {
        return (String) values.get(ABV);
    }

    public void setAbv(String abv) {
        values.put(ABV, abv);
    }

    public String getIbu() {
        return (String) values.get(IBU);
    }

    public void setIbu(String ibu) {
        values.put(IBU, ibu);
    }

    public String getCity() {
        return (String) values.get(CITY);
    }

    public void setCity(String city) {
        values.put(CITY, city);
    }

    public String getState() {
        return (String) values.get(STATE);
    }

    public void setState(String state) {
        values.put(STATE, state);
    }

    public String getCountry() {
        return (String) values.get(COUNTRY);
    }

    public void setCountry(String country) {
        values.put(COUNTRY, country);
    }

    public String getRating() {
        return (String) values.get(RATING);
    }

    public void setRating(String rating) {
        values.put(RATING, rating);
    }

    public String getUser() {
        return (String) values.get(USER);
    }

    public void setUser(String user) {
        values.put(USER, user);
    }

    public String getClassifiedBrewery() {
        return (String) values.get(CLASSIFIED_BREWERY);
    }

    public void setClassifiedBrewery(String classifiedBrewery) {
        values.put(CLASSIFIED_BREWERY, classifiedBrewery);
    }

    public String getClassifiedName() {
        return (String) values.get(CLASSIFIED_NAME);
    }

    public void setClassifiedName(String classifiedName) {
        values.put(CLASSIFIED_NAME, classifiedName);
    }

    public String getClassifiedAbv() {
        return (String) values.get(CLASSIFIED_ABV);
    }

    public void setClassifiedAbv(String classifiedAbv) {
        values.put(CLASSIFIED_ABV, classifiedAbv);
    }

    public String getClassifiedIbu() {
        return (String) values.get(CLASSIFIED_IBU);
    }

    public void setClassifiedIbu(String classifiedIbu) {
        values.put(CLASSIFIED_IBU, classifiedIbu);
    }

    public String getClassifiedCity() {
        return (String) values.get(CLASSIFIED_CITY);
    }

    public void setClassifiedCity(String classifiedCity) {
        values.put(CLASSIFIED_CITY, classifiedCity);
    }

    public String getClassifiedState() {
        return (String) values.get(CLASSIFIED_STATE);
    }

    public void setClassifiedState(String classifiedState) {
        values.put(CLASSIFIED_STATE, classifiedState);
    }

    public String getClassifiedCountry() {
        return (String) values.get(CLASSIFIED_COUNTRY);
    }

    public void setClassifiedCountry(String classifiedCountry) {
        values.put(CLASSIFIED_COUNTRY, classifiedCountry);
    }

    public String getClassifiedRating() {
        return (String) values.get(CLASSIFIED_RATING);
    }

    public void setClassifiedRating(String classifiedRating) {
        values.put(CLASSIFIED_RATING, classifiedRating);
    }

    public String getClassifiedUser() {
        return (String) values.get(CLASSIFIED_USER);
    }

    public void setClassifiedUser(String classifiedUser) {
        values.put(CLASSIFIED_USER, classifiedUser);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder csvRow = new StringBuilder();

        csvRow.append(ID);
        csvRow.append(',').append(getBrewery());
        csvRow.append(',').append(getName());
        csvRow.append(',').append(getAbv());
        csvRow.append(',').append(getIbu());
        csvRow.append(',').append(getCity());
        csvRow.append(',').append(getState());
        csvRow.append(',').append(getCountry());
        csvRow.append(',').append(getRating());
        csvRow.append(',').append(getUser());

        return csvRow.toString();
    }

    @Override
    public void save() {
        dao.save(this);

    }

}
