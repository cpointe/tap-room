package org.bitbucket.taproom;

import java.util.ArrayList;
import java.util.Collection;

public class BeerRatingCsvFile {

    public Collection<BeerRating> records = new ArrayList<>();

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder csvDocument = new StringBuilder();
        
        csvDocument.append("id,brewery,name,'abv,ibu,city,state,country,rating,user\n");
        
        for (BeerRating record : records) {
            csvDocument.append(record.toString()).append('\n');
        }
        
        
        return csvDocument.toString();
    }
    
}
