package org.bitbucket.taproom;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/specifications", plugin="json:target/cucumber-html-reports/cucumber.json")
public class CucumberIT {

}
