@classifyData
Feature: Classification

  Scenario: Load a csv file for classification
    When a csv file is loaded for classification
    Then it is available for classification
    And each row in the csv file is available as a unique document

  Scenario Outline: Leverage synonym matching for classification
    Given an index classified to process full state names as synonyms with state abbreviations
    When a csv file is loaded and classified that contains the state name "<stateName>"
    Then the document is classified with the state abbreviation "<stateAbbreviation>"

    Examples: 
      | stateName  | stateAbbreviation |
      | Delaware   | DE                |
      | California | CA                |
      | CA         | CA                |
      | Calif      | CA                |

  Scenario Outline: Leverage spelling conversion matching for classification (synonym conversions to fix common misspellings)
    Given an index classified to process full state names as synonyms with state abbreviations
    When a csv file is loaded and classified that contains the state name "<stateName>"
    Then the document is classified with the state abbreviation "<stateAbbreviation>"

    Examples: 
      | stateName | stateAbbreviation |
      | Deleware  | DE                |
      | Minnisota | MN                |

  Scenario Outline: Leverage attribute defaulting to populate classification values
    Given an index classified to process brewery names so that matches to "<rawBrewery>" have the following values defaulted:
      | classifiedBrewery |
      | classifiedCity    |
      | classifiedState   |
      | classifiedCountry |
    When a csv file is loaded and classified that contains the brewery name "<rawBrewery>"
    Then the document contains the following brewery information values defaulted:
      | ClassifiedBrewery   | classifiedCity   | classifiedState   | classifiedCountry   |
      | <classifiedBrewery> | <classifiedCity> | <classifiedState> | <classifiedCountry> |

    Examples: 
      | rawBrewery   | classifiedBrewery | classifiedCity | classifiedState | classifiedCountry |
      | Dogfish Head | Dogfish Head      | Milton         | DE              | USA               |
      | DFH          | Dogfish Head      | Milton         | DE              | USA               |
      | Ale Smith    | Alesmith          | San Diego      | CA              | USA               |
