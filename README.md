# Tap Room - Light Weight, Lucene-Driven Correlation #
[![Maven Central](https://img.shields.io/maven-central/v/org.bitbucket.cpointe.tap-room/tap-room.svg)](https://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22org.bitbucket.cpointe.tap-room%22%20AND%20a%3A%tap-room%22)
[![License](https://img.shields.io/github/license/mashape/apistatus.svg)](https://opensource.org/licenses/mit)

Tap Room is a simple extraction and correlation framework that leverages Lucene-based queries to work with small dataset. There are better ways to handle "macro" or "micro" sized data sets 
(e.g., Apache Spark). This project is aimed at "nano" sized data sets where adding more components isn't possible or worth the effort.

# Documentation#

Please see our [wiki for documentation](https://fermenter.atlassian.net/wiki/spaces/TR/overview?homepageId=486408314)

# Distribution Channel

Want Tap Room in your project? The following Maven dependency will add it to your Maven project from the Maven Central Repository:

```
#!xml
<dependency>
    <groupId>org.bitbucket.cpointe.taproom</groupId>
    <artifactId>tap-room-service</artifactId>
    <version>0.1.0-SNAPSHOT</version>
</dependency>
```

## Releasing to Maven Central Repository

Krausening uses both the `maven-release-plugin` and the `nexus-staging-maven-plugin` to facilitate the release and deployment of new Tap Room builds. In order to perform a release, you must:

1.) Obtain a [JIRA](https://issues.sonatype.org/secure/Dashboard.jspa) account with Sonatype OSSRH and access to the `org.bitbucket.cpointe` project group

2.) Ensure that your Sonatype OSSRH JIRA account credentials are specified in your `settings.xml`:

```
#!xml
<settings>
  <servers>
    <server>
      <id>ossrh</id>
      <username>your-jira-id</username>
      <password>your-jira-pwd</password>
    </server>
  </servers>
</settings>
```

3.) Install `gpg` and distribute your key pair - see [here](http://central.sonatype.org/pages/working-with-pgp-signatures.html). OS X users may need to execute:

```
#!bash
export GPG_TTY=`tty`;
```

4.) Execute `mvn release:clean release:prepare`, answer the prompts for the versions and tags, and perform `mvn release:perform`

## Licensing
Krausening is available under the [MIT License](http://opensource.org/licenses/mit-license.php).

## Session Beer
Tap Room would like to thank [Counterpointe Solutions](http://cpointe-inc.com/) for providing continuous integration services.
