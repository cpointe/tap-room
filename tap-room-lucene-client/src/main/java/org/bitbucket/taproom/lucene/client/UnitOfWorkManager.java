package org.bitbucket.taproom.lucene.client;

/**
 * Looks up the current implementation of the unit of work (e.g., Solr, Elastisearch).
 */
public final class UnitOfWorkManager {

    private static UnitOfWork unitOfWork = getUnitOfWork();

    private UnitOfWorkManager() {
        // prevent instantiation outside this class
    }

    /**
     * Returns the current unit of work implementation.
     * 
     * @return unit of work
     */
    public static UnitOfWork getUnitOfWork() {
        if (unitOfWork == null) {
            // TODO: TR-2: externalize to support solr or elastisearch based on a property:
            String tapRoomClient = "org.bitbucket.taproom.solr.SolrUnitOfWork";
            unitOfWork = (UnitOfWork) TapRoomUtils.getInstanceOfClass(tapRoomClient);
        }
        return unitOfWork;

    }

}
