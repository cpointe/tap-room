package org.bitbucket.taproom.lucene.client;

import java.io.File;
import java.util.Collection;

/**
 * Contract for an {@link IndexItem} data access object
 * 
 * @param <T>
 *            a subclass of {@link IndexItem} with the specific getters and logic needed for your classification routine
 */
public interface IndexItemDaoImpl<T extends IndexItem> {

    /**
     * Adds a file with the passed contextType to the index.
     * 
     * @param file
     *            file to add
     * @param contentType
     *            content type
     * @return true if successful
     */
    boolean addToIndex(File file, String contentType);

    /**
     * Saves (creates or updates) the passed item to the underlying index.
     * 
     * @param indexItem
     *            item to save
     */
    void save(T indexItem);

    /**
     * Queries the index for the passed search terms.
     * 
     * @param queryString
     *            desired search terms
     * @param startIndex
     *            first result to return
     * @param pageSize
     *            number of results to return
     */
    Collection<T> query(String queryString, long startIndex, long pageSize, Class<T> responseTypeClass);

    /**
     * Deletes anything that matches the passed query.
     * 
     * @param queryString
     *            string to match
     */
    void delete(String queryString);

}
