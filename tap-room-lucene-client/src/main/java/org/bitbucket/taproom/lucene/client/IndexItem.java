package org.bitbucket.taproom.lucene.client;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Represents a typed version of a an object that is being classified via an indexing engine (e.g., Solr, Elastisearch).
 * 
 * This follows an "object bean" pattern that where everything is stored in a map for easier use with classification.
 */
public abstract class IndexItem {

    protected Map<String, Object> values = new HashMap<>();

    /**
     * Creates a new instance without any raw value anchoring.
     */
    public IndexItem() {

    }

    /**
     * Creates a new index with a set of raw values. Typically, this is a map of key value pairs from a indexing engine.
     * 
     * @param rawIndexValues
     */
    public IndexItem(Map<String, Object> rawIndexValues) {
        addAllValues(rawIndexValues);
    }

    /**
     * Allows all values to be updated via the underlying map that contains all instance variable values.
     * 
     * @param rawIndexValues
     *            values to set
     */
    public void setValues(Map<String, Object> rawIndexValues) {
        addAllValues(rawIndexValues);
    }

    /**
     * Gets all values in the underlying map that contains all instance variable values.
     * 
     * @return values for this object
     */
    public Map<String, Object> getValues() {
        return values;
    }

    /**
     * We can't guarantee that a Map returned from a lucene index will implement the entire interface (it does not, for
     * instance, in Solr). So copy these the old fashioned way.
     * 
     * @param rawIndexValues
     *            map to copy into the values member variable
     */
    private void addAllValues(Map<String, Object> rawIndexValues) {
        for (String key : rawIndexValues.keySet()) {
            values.put(key, rawIndexValues.get(key));
        }
    }

    /**
     * Returns the id of this instance.
     * 
     * @return id
     */
    public abstract String getId();

    /**
     * Saves the item to the index (create or update).
     */
    public abstract void save();

    /**
     * Convenience method that will call the appropriate getter for each field so any 'get' logic can be performed.
     * 
     * @param fieldName
     *            the field naem to get
     * @return the response from get<CapitalizedFieldName>()
     */
    public String get(String fieldName) {
        try {
            Class<?>[] args = {};
            Method m = getClass().getMethod("get" + StringUtils.capitalize(fieldName), args);
            return (String) m.invoke(this);

        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new TapRoomClientException("Could not invoke getter for field: " + fieldName, e);
        }
    }

    /**
     * Returns the list of class fields that should be persisted. By default, will use reflection and persist all
     * fields.
     * 
     * @return fields to be persisted in the index
     */
    public Collection<String> getIndexedFields() {
        Field[] fields = getClass().getFields();

        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields) {
            fieldNames.add(field.getName());
        }

        return fieldNames;

    }

}
