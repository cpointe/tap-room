package org.bitbucket.taproom.lucene.client;

/**
 * Runtime exception for use within Tap Room.
 */
public class TapRoomClientException extends RuntimeException {

    private static final long serialVersionUID = 1003593172742362374L;

    /**
     * {@inheritDoc}
     */
    public TapRoomClientException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public TapRoomClientException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public TapRoomClientException(String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public TapRoomClientException(Throwable cause) {
        super(cause);
    }

}
