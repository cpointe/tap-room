package org.bitbucket.taproom.lucene.client;

import java.io.File;
import java.util.Collection;

/**
 * Generic data access object functions for {@link IndexItem} instances.
 *
 * @param <T>
 *            a subclass of {@link IndexItem} with the specific getters and logic needed for your classification routine
 */
public class IndexItemDao<T extends IndexItem> implements IndexItemDaoImpl<T> {

    private IndexItemDaoImpl<T> daoImpl = getDaoImplementation();

    /**
     * Returns the configured implementation being used (e.g., solr, elasticsearch).
     * 
     * @return current dao implementation
     */
    private IndexItemDaoImpl<T> getDaoImplementation() {
        // TODO: externalize to support solr or elastisearch based on a property:
        String tapRoomClient = "org.bitbucket.taproom.solr.SolrIndexItemDao";
        IndexItemDaoImpl<T> daoImplementation = (IndexItemDaoImpl<T>) TapRoomUtils.getInstanceOfClass(tapRoomClient);
        return daoImplementation;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addToIndex(File file, String contentType) {
        return daoImpl.addToIndex(file, contentType);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save(T indexItem) {
        daoImpl.save(indexItem);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<T> query(String queryString, long startIndex, long pageSize, Class<T> responseTypeClass) {
        return daoImpl.query(queryString, startIndex, pageSize, responseTypeClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String queryString) {
        daoImpl.delete(queryString);
    }

}
