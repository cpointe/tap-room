package org.bitbucket.taproom.lucene.client;

/**
 * Utilities for Tap Room.
 */
public final class TapRoomUtils {

    private TapRoomUtils() {
        // prevent instantiation of all static class
    }

    /**
     * Instantiate a dynamic class. Throws a TapRoomClientException on any issues encountered.
     * 
     * @param className
     *            name of the class
     * @return the instantiated object
     */
    public static Object getInstanceOfClass(String className) {
        try {
            Class<?> clazz = Class.forName(className);
            return clazz.newInstance();

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            throw new TapRoomClientException("Could not instantiate tap-room client: " + className, e);

        }

    }

    /**
     * Instantiate a dynamic class so exception handling isn't needed. Throws a TapRoomClientException on any issues
     * encountered.
     * 
     * @param clazz
     *            class
     * @return the instantiated object
     */
    public static <T> T getInstanceOfClass(Class<T> clazz) {
        try {
            return clazz.newInstance();

        } catch (InstantiationException | IllegalAccessException e) {
            throw new TapRoomClientException("Could not instantiate tap-room client: " + clazz.getName(), e);

        }

    }

}
