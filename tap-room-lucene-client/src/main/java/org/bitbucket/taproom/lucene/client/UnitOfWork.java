package org.bitbucket.taproom.lucene.client;

/**
 * Represents unit of work (i.e., a transaction) within TapRoom.
 */
public interface UnitOfWork {

    /**
     * Adds an item to the current unit of work.
     * 
     * @param id
     *            identified for the item
     * @param indexItem
     *            the item
     */
    void add(String id, IndexItem indexItem);

    /**
     * Adds an item to the current unit of work.
     * 
     * @param id
     *            identified for the item
     * @param workItem
     *            the underlying index item for underlying implementation (e.g., solr document)
     * @param indexItem
     *            the item
     */
    void add(String id, Object workItem, IndexItem indexItem);

    /**
     * Returns the item for the resulted id or null if none is know by the unit of work.
     * 
     * @param id
     *            identified for the requested item
     * @return the item or null
     */
    IndexItem getItem(String id);

    /**
     * Pushes all pending items in the unit of work to the underlying Lucene index implementation.
     */
    void flush();

    /**
     * Commits all pending or previously flushed items in the unit of work to the underlying Lucene index implemenation.
     */
    void commit();

}
