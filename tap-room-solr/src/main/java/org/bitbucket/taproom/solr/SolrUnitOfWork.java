package org.bitbucket.taproom.solr;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.aeonbits.owner.KrauseningConfigFactory;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient.Builder;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.bitbucket.taproom.lucene.client.IndexItem;
import org.bitbucket.taproom.lucene.client.TapRoomClientException;
import org.bitbucket.taproom.lucene.client.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Solr implementation of the Tap Room {@link UnitOfWork}.
 */
public class SolrUnitOfWork implements UnitOfWork {

    private static final Logger LOGGER = LoggerFactory.getLogger(SolrUnitOfWork.class);

    private SolrClient solrClient = null;

    private Map<String, SolrInputDocument> workItems = new HashMap<>();
    private Map<String, IndexItem> indexItems = new HashMap<>();

    private SolrClientConfig config = KrauseningConfigFactory.create(SolrClientConfig.class);

    /**
     * New instance using the values in {@link SolrClientConfig}.
     */
    public SolrUnitOfWork() {
        Builder clientBuilder = new Builder(config.getSolrBaseURL());
        solrClient = clientBuilder.build();
        LOGGER.debug("Configured (but not invoked) connection to Solr at {}", config.getSolrBaseURL());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(String id, IndexItem indexItem) {
        indexItems.put(id, indexItem);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexItem getItem(String id) {
        return indexItems.get(id);

    }

    public void add(String id, SolrInputDocument workItem, IndexItem indexItem) {
        workItems.put(id, workItem);
        add(id, indexItem);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() {
        if (workItems.size() >= 1000) {
            commit();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void commit() {
        commit(false);
    }

    /**
     * Commits any un-committed {@link SolrInputDocument}s that have been added to this unit of work and optionally
     * instructs Solr to build internal data structures needed to provide suggestion capabilities.
     * 
     * @param buildSuggesterIndex
     *            whether or not to additionally instruct Solr to build internal lookup data structures that enable
     *            suggestions on index terms. <b>NOTE:</b> Depending on the index size, rebuilding suggestion indexes
     *            may take a long time, and thereby it's usage should be carefully considered.
     */
    public void commit(boolean buildSuggesterIndex) {
        try {
            int itemsInUnitOfWork = workItems.size();
            LOGGER.debug("Committing {} to Solr...", itemsInUnitOfWork);
            if (itemsInUnitOfWork > 0) {
                solrClient.add(workItems.values());
            }
            solrClient.commit();
            workItems.clear();
            indexItems.clear();
            LOGGER.debug("Committing - COMPLETED");

            if (buildSuggesterIndex) {
                LOGGER.debug("START - Building Solr tap-room suggester lookup data structures...");
                SolrQuery buildSuggesterQuery = new SolrQuery();
                buildSuggesterQuery.setRequestHandler(config.getSolrSuggestRequestHandler());
                buildSuggesterQuery.setParam("suggest.build", "true");
                solrClient.query(buildSuggesterQuery);
                LOGGER.debug("COMPLETE - tap-room suggester lookup data structures");
            }
        } catch (SolrServerException | IOException e) {
            throw new TapRoomClientException("Problem commiting to Solr!", e);
        }
    }

    SolrClient getSolrClient() {
        return solrClient;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(String id, Object workItem, IndexItem indexItem) {
        add(id, (SolrDocument) workItem, indexItem);

    }

}
