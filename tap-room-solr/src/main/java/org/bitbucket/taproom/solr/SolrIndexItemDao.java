package org.bitbucket.taproom.solr;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.request.ContentStreamUpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.bitbucket.taproom.lucene.client.IndexItem;
import org.bitbucket.taproom.lucene.client.IndexItemDaoImpl;
import org.bitbucket.taproom.lucene.client.TapRoomClientException;
import org.bitbucket.taproom.lucene.client.TapRoomUtils;
import org.bitbucket.taproom.lucene.client.UnitOfWorkManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Solr implemantation of the Tap Room data access object.
 */
public class SolrIndexItemDao<T extends IndexItem> implements IndexItemDaoImpl<T> {

    private static final Logger logger = LoggerFactory.getLogger(SolrIndexItemDao.class);

    private static final String LITERAL = "literal.";

    private static SolrUnitOfWork unitOfWork = (SolrUnitOfWork) UnitOfWorkManager.getUnitOfWork();

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addToIndex(File file, String contentType) {
        boolean success = false;

        ContentStreamUpdateRequest up = new ContentStreamUpdateRequest("/update");
        try {
            up.addFile(file, contentType);

        } catch (IOException ioe) {
            logger.error("Could not access ingest file: " + file, ioe);

        }

        up.setParam(LITERAL + "sourceFile", file.getName());
        up.setParam("overwrite", "true");

        SolrClient solrClient = unitOfWork.getSolrClient();
        try {
            solrClient.request(up);
            success = true;
        } catch (SolrServerException | IOException e) {
            logger.error("Could not process ingest file: " + file, e);

        }

        return success;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void save(T indexItem) {
        if (indexItem != null) {

            String id = indexItem.getId();
            SolrInputDocument document = new SolrInputDocument();

            document.setField("id", id);
            logger.debug("Saving to solr document with id {}", id);

            for (String fieldName : indexItem.getIndexedFields()) {
                document.setField(fieldName, indexItem.get(fieldName));
                logger.debug("Saving to solr {} with value {}", fieldName, indexItem.get(fieldName));
            }

            unitOfWork.add(id, document, indexItem);

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<T> query(String queryString, long startIndex, long pageSize, Class<T> responseTypeClass) {
        Collection<T> indexItems = new ArrayList<>();

        SolrDocumentList results = executeSolrQuery(queryString, startIndex, pageSize);
        for (SolrDocument result : results) {
            T indexItem = addResultToUnitOfWork(result, responseTypeClass);
            indexItems.add(indexItem);

        }
        long totalNumResults = results.getNumFound();

        logger.debug("Processed {} results for query: {}", totalNumResults, queryString);

        return indexItems;
    }

    private SolrDocumentList executeSolrQuery(String query, long startIndex, long numResultsToReturn) {
        SolrClient solrClient = unitOfWork.getSolrClient();
        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set(CommonParams.Q, query);
        params.set(CommonParams.ROWS, String.valueOf(numResultsToReturn));
        params.set(CommonParams.START, String.valueOf(startIndex));
        QueryResponse queryResponse;
        try {
            queryResponse = solrClient.query(params);

        } catch (SolrServerException | IOException e) {
            throw new TapRoomClientException("Issue executing query: " + query, e);

        }

        return queryResponse.getResults();
    }

    private T addResultToUnitOfWork(SolrDocument result, Class<T> responseTypeClass) {
        T indexItem = TapRoomUtils.getInstanceOfClass(responseTypeClass);
        // create and pass in map:
        if (indexItem != null) {
            Map<String, Object> solrValues = result.getFieldValueMap();
            String id = (String) solrValues.get("id");
            T existingInstance = (T) unitOfWork.getItem(id);
            if (existingInstance != null) {
                indexItem = existingInstance;
            } else {

                indexItem.setValues(solrValues);
            }

            unitOfWork.add(id, indexItem);

        } else {
            logger.warn("Result could not be added to unit of work!\n\t{}", result);
        }

        return indexItem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String queryString) {
        SolrClient solrClient = unitOfWork.getSolrClient();
        try {
            solrClient.deleteByQuery(queryString);
            unitOfWork.commit();
        } catch (SolrServerException | IOException e) {
            throw new TapRoomClientException("Could not delete by query string: " + queryString, e);
        }

    }

}
