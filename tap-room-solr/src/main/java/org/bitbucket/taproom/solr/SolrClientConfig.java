package org.bitbucket.taproom.solr;

import org.aeonbits.owner.KrauseningConfig;
import org.aeonbits.owner.Config.HotReload;
import org.aeonbits.owner.Config.HotReloadType;
import org.aeonbits.owner.KrauseningConfig.KrauseningSources;

/**
 * Externalized Solr configuration properties.
 */
@KrauseningSources("tap-room.properties")
@HotReload(type = HotReloadType.SYNC)
public interface SolrClientConfig extends KrauseningConfig {

    /**
     * Returns the based URL to the tap-room solr core.
     * 
     * @return url
     */
    @Key("solr.base.url")
    @DefaultValue("http://localhost:8983/solr/tap-room")
    String getSolrBaseURL();

    /**
     * Name of the id field leveraged by solr.
     * 
     * @return id field name
     */
    @Key("solr.fields.unique-key")
    @DefaultValue("id")
    String getSolrDocUniqueKeyField();

    /**
     * Delimiter used to create composite values within the solr id.
     * 
     * @return delimiter
     */
    @Key("solr.id.delimiter")
    @DefaultValue("^")
    String getIdDelimiter();

    /**
     * Name of the suggester that tap-rooom should use.
     * 
     * @return suggester relative path on the base solr url
     */
    @Key("solr.suggest.request-handler")
    @DefaultValue("/suggest")
    String getSolrSuggestRequestHandler();

}
